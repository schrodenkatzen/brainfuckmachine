Simple brainfuck language interpreter for python.

<a href="https://learnxinyminutes.com/docs/bf/">Learn how to code brainfuck</a>
brainfuckmachine.execute("++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.", mode="char")
#"Hello world!"
brainfuckmachine.execute(",>,< [ > [ >+ >+ << -] >> [- << + >>] <<< -] >>.",input=[2,3],mode="int")
#6
brainfuckmachine.execute([5, 3, 5, 2, [3, [3, 0, 3, 0, 2, 2, 1], 3, 3, [1, 2, 2, 0, 3, 3], 2, 2, 2, 1], 3, 3, 4]) # 012345[] instead of +-<>.,[]
#6