import setuptools
import requests

with open("README.md", "r") as fh:

    long_description = fh.read()
    name="brainfuckmachine"
    version="0.1.1.25"
    remote_version=requests.get("https://pypi.org/pypi/%s/json" % name).json()['info']['version']
    if version<=remote_version:
      version=remote_version.split('.')
      version[-1]=str(int(version[-1])+1)
      version=".".join(version)

    setuptools.setup(

       name=name,  

       version=version,

       packages=['brainfuckmachine'],

       author="Vlad Havrilov",

       author_email="wladgavrilov@gmail.com",

       description="Simple Brainfuck compiler for python",

       long_description=long_description,

       long_description_content_type="text/markdown",

       url="https://bitbucket.org/schrodenkatzen/brainfuckmachine/src/master/",

     #packages=setuptools.find_packages(),
     install_requires=[
     'hy',
     ],
     package_data={'': ['brainfuckmachine.hy']},
     include_package_data=True,
     classifiers=[

     "Programming Language :: Python :: 3",

     "License :: OSI Approved :: MIT License",

     "Operating System :: OS Independent",

     ],

     )
